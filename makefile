cc=gcc
cf=-march=native -mtune=native -O2
af=-masm=intel -S
seltok:	./seltok.c
	$(cc) $(cf) ./seltok.c -o ./seltok
asm:	./seltok.c
	$(cc) $(cf) $(af) ./seltok.c -o ./seltok
install:	./seltok
	sudo cp ./seltok /usr/bin/seltok
unintall:
	sudo rm /usr/bin/seltok
clean:	./seltok
	rm ./seltok
cleanasm: 
	rm ./seltok.s
