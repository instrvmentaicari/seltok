Simple program that tokenizes lines by spaces and tabs, then emits only the selected index of each line.
I wrote this because it's literally easier to program your way around learning anything to do with sed or awk.
