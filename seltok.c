#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"

int main (int argc, char** argv){
 char c, nl= 0, et= 0, at= 0, delim= argc>2? argv[2][0]: 0;
 int i= 0,t= argc>1? atoi (argv[1]): 0;
 while (read (0, &c, 1)){
  et|= (delim!=0? c==delim: c==' '|| c=='\t')|| i>t? 1: 0;
  if (et&c!='\n'&at) write (1, "\n", 1);
  at= i==t&!et? write (1, &c, 1), 1: 0;
  nl= c=='\n'? et= 0, i= 0, 1: 0;
  i+= et&i<=t? et= 0, 1: 0; 
 }
 return 0;
}
